PREFIX = /usr/local

.PHONY: install

install: "$(PREFIX)/bin"
	install mpbackup "$(PREFIX)/bin/mpbackup"

"$(PREFIX)/bin":
	mkdir -p "$(PREFIX)/bin"
