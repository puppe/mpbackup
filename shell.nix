{ pkgs ? import <nixpkgs> {} }:
let
  commonAttrs = import ./common-attrs.nix pkgs;
in
pkgs.mkShell {
  inherit (commonAttrs) buildInputs;
}
